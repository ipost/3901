#! /bin/bash
curl http://ocio.osu.edu/software/directory/slwin/ 2> /dev/null > temp.out
echo "List of software offered by the OCIO at OSU"
echo "Enter search term to filter software:"
read key
if [ "$key" == "" ]; then
	key='.*'
	echo "All software offered by the OCIO :
"
else
	echo "Software filtered by '$key':
"
fi

cat temp.out | sed ':a;N;$!ba;s/\n\|(\r\n)/1x2y3z/g' | sed 's/<\!doc.*<\/a><br\/><\/td>1x2y3z<\/tr><\/tbody><\/table>//g' | sed 's/<hr\/><p><strong>CONTACT.*<\/html>//g' | grep -P -o "<h2>(<strong>)?(<a id=\".*?\" name=\".*?\">(.*?)</a>)?(.*?)(.*?):?(</strong>)?</h2>" | sed s/\>['\s']+\</\>\</g | sed s/1x2y3z/'\n'/g  | sed s/:\s*\</\</g | sed s/\<[^\>]*\>//g | sed 's/^[\xc2\xa0 ]*//g' | sed 's/\&amp;/\&/g' | grep -i "$key"

rm temp.out
